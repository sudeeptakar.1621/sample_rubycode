require_relative 'add_num'

RSpec.describe 'AddNum' do
  it 'should return the sum of two numbers' do
    allow_any_instance_of(Kernel).to receive(:gets).and_return("5\n", "3\n")
    expect(AddNum()).to eq(8)
  end

  it 'should return 0 when adding two zeros' do
    allow_any_instance_of(Kernel).to receive(:gets).and_return("0\n", "0\n")
    expect(AddNum()).to eq(0)
  end

  it 'should return the correct sum for negative numbers' do
    allow_any_instance_of(Kernel).to receive(:gets).and_return("-5\n", "-3\n")
    expect(AddNum()).to eq(-8)
  end
end
